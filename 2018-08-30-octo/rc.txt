set MODE custom

uorb start

if sercon
then
	echo "USB connected"
fi

usleep 1000000

param set MAV_TYPE 2

param select /fs/microsd/params
if [ -f /fs/microsd/params ]
then
	if param load /fs/microsd/params
	then
		echo "Parameters loaded"
	else
		echo "Parameter file corrupt - ignoring"
	fi
fi


if rgbled start
then
	echo "Using external RGB Led"
else
	if blinkm start
	then
		blinkm systemstate
	fi
fi

if px4io detect
then
	echo "PX4IO running, not upgrading"
else
	echo "Attempting to upgrade PX4IO"
	if px4io update
	then
		if [ -d /fs/microsd ]
		then
			echo "Flashed PX4IO Firmware OK" > /fs/microsd/px4io.log
		fi
 
		# Allow IO to safely kick back to app
		usleep 200000
	else
		echo "No PX4IO to upgrade here"
	fi
fi

px4io start

sh /etc/init.d/rc.io

sh /etc/init.d/rc.sensors

gps start 

fmu mode_pwm

commander start

attitude_estimator_ekf start

position_estimator_inav start

mixer load /dev/pwm_output /etc/mixers/IO_pass.mix
#mixer load /dev/px4fmu /etc/mixers/gimbal_aux.mix

mc_att_control start
mc_pos_control start

dataman start
stats start
preflightcheck start

sdlog2 start -r 10 -a -b 16


pwm rate -g 0 -g 2 -r 400

nshterm /dev/ttyACM0 &


mavlink start -d /dev/ttyS2 -b 230400

#px4flow start
#gimbal_aux start

sf0x start

usleep 5000

#exit





